# redis-repo-key-fetcher

## To install

1. Make sure you are running at least go 1.17
2. Add your GOBIN dir to your $PATH (`export PATH=$PATH:$(go env GOPATH)/bin`)
2. `go install ./...`
4. Re-initialize your terminal or re-source your shell .rc file (e. source ~/.bashrc) if you've done step 2 inside your shell's rc file
5. Call `redis-repo-key-fetcher` from your new/re-sourced terminal
