module gitLab.com/suleimiahmed/scratchpad/redis-repo-key-fetcher

go 1.19

require github.com/go-redis/redis/v8 v8.11.5

require (
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781 // indirect
	google.golang.org/appengine v1.6.7 // indirect
)
